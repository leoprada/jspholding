
package Modelos;

/**
 *
 * @author Martika
 */
public class ModAsesor {

    public int getIdasesor() {
        return idasesor;
    }

    public void setIdasesor(int idasesor) {
        this.idasesor = idasesor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombres) {
        this.nombre = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
    
    private int idasesor;
    private String nombre;
    private String apellidos;
    private String telefono;
    private String direccion;
    private String titulo;
    @SuppressWarnings("FieldMayBeFinal")
    private char estado;
    
    public ModAsesor () {
        estado = '1';
    }
}
