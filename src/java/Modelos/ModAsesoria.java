
package Modelos;

/**
 *
 * @author Martika
 */
public class ModAsesoria {

    public int getIdasesoria() {
        return idasesoria;
    }

    public void setIdasesoria(int idasesoria) {
        this.idasesoria = idasesoria;
    }

    public int getAsesor() {
        return asesor;
    }

    public void setAsesor(int asesor) {
        this.asesor = asesor;
    }

    public int getEmpresa() {
        return empresa;
    }

    public void setEmpresa(int empresa) {
        this.empresa = empresa;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    
    private int idasesoria;
    private int asesor;
    private int empresa;
    private String fechaInicio;
    
}
