
package Modelos;

/**
 *
 * @author Martika
 */
public class ModEmpresa {

    public int getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(int idempresa) {
        this.idempresa = idempresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private int idempresa;
    private String nombre;
}
