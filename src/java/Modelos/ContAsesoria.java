package Modelos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class ContAsesoria 
{
    private Connection cnx;
    
    public boolean Conexion() throws ClassNotFoundException 
    {
       boolean conecto=false;
       try{
           Class.forName("com.mysql.jdbc.Driver");
           String db = "jdbc:mysql://localhost/holding";
           cnx = DriverManager.getConnection(db, "root", "");
           conecto=true;
       }catch(SQLException e)
       {
           e.getMessage();
           System.out.println(e);
       }
       return conecto;
    }
    
    public void Cerrar() throws SQLException
    {
        cnx.close();
    }
    
    public Boolean InsertarArea(ModAsesoria Nuevo)
    {
        try {
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            int fila;
            int idasesoria, idasesor, idempresa;
            String fechaInicio;
               
            idasesoria=Nuevo.getIdasesoria();
            idasesor=Nuevo.getAsesor();
            idempresa=Nuevo.getEmpresa();
            fechaInicio=Nuevo.getFechaInicio();
            
            sentencia = "Select * from asesoria where idasesoria=" + idasesoria;
            
            tabla=sql.executeQuery(sentencia);
            if(!tabla.next())
            {
                sentencia = "INSERT INTO asesoria (idasesoria, asesor, empresa, fechaInicio) "
                    + "VALUES ('" + idasesoria + "', '"+ idasesor +"', '"+ idempresa +"', '"+ fechaInicio + "')";
                 fila=sql.executeUpdate(sentencia);
                 if(fila == 1)
                     return true;
            }
            else
            {
                System.out.println("Este ID ya existe");
                return false;
            }
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
            return false;
        }
        return false;
    }
    
    
     public int BorrarArea(int id)
     {
        int fila = 0;
         try{
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            sentencia="Select * from Asesoria where idasesoria="+ id;
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                 sentencia="Delete from Asesoria where idasesoria="+ id;
                 fila=sql.executeUpdate(sentencia);
                 return fila;
            }
            else
                System.out.println("Id no existe");
            return fila;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
            return fila;
        }
     }
     
    
     public JSONArray MostrarunArea1(int id)
     {
         ModAsesoria Nuevo=new ModAsesoria(); 
         JSONArray tablaJSON = new JSONArray();
         try{
            Statement sql;
                sql = cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            sentencia="Select a.*, asesor.nombres asesor_nombre, em.nombre empresa_nombre from asesoria a left join asesor on a.asesor = asesor.idasesor left join empresa em on em.idempresa = a.empresa where idasesoria='" + id + "'";
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                tablaJSON.add(Integer.parseInt(tabla.getString("idasesoria")));
                tablaJSON.add(Integer.parseInt(tabla.getString("asesor")));
                tablaJSON.add(tabla.getString("asesor_nombre"));
                tablaJSON.add(Integer.parseInt(tabla.getString("empresa")));
                tablaJSON.add(tabla.getString("empresa_nombre"));
                tablaJSON.add(tabla.getString("fechaInicio"));
            }
            //else
            //    Nuevo=null;
            } catch (SQLException e) 
            {
                e.getErrorCode();
                System.out.println(e);
            } 
        return tablaJSON;
     }
     
     public JSONArray MostrarTodo()
     {
        JSONArray tablaJSON = new JSONArray();
       try{
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            int idasesor, idempresa;
            String fechainicio, nameAsesor, nameEmpresa;
            
            sentencia="Select a.*, e.nombre empresanombre, asesor.nombres asesorname from asesoria a"
                    + " left join empresa e on a.empresa = e.idempresa"
                    + " left join asesor on a.asesor = asesor.idasesor";
            tabla=sql.executeQuery(sentencia);
            
            while(tabla.next())
            {
                JSONObject obj = new JSONObject();
                idasesor = Integer.parseInt(tabla.getString("asesor"));
                nameAsesor = tabla.getString("asesorname");
                idempresa = Integer.parseInt(tabla.getString("empresa"));
                nameEmpresa = tabla.getString("empresanombre");
                fechainicio = tabla.getString("fechaInicio");
                obj.put("idasesoria", Integer.parseInt(tabla.getString("idasesoria")));
                obj.put("asesor_id", idasesor);
                obj.put("asesor_nombre", nameAsesor);
                obj.put("empresa_id", idempresa);
                obj.put("empresa_nombre", nameEmpresa);
                obj.put("fechainicio", fechainicio);
                tablaJSON.add(obj);
            }
            return tablaJSON;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            return tablaJSON;
        }
     }
     
     public int ModificarArea1(ModAsesoria Nuevo)
    {
      int fila=0;  
      try {
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            int idasesoria, idasesor, idempresa;
            String fechaInicio;
               
            idasesoria=Nuevo.getIdasesoria();
            idasesor=Nuevo.getAsesor();
            idempresa=Nuevo.getEmpresa();
            fechaInicio=Nuevo.getFechaInicio();
               
            
            sentencia="Select * from Asesoria where idasesoria="+ idasesoria;
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                sentencia="Update Asesoria set asesor='" + idasesor +"', " +
                "empresa='" + idempresa +"', " +
                "fechaInicio='" + fechaInicio +"'" +
                " where idasesoria=" + idasesoria;
                
                fila=sql.executeUpdate(sentencia);
            }
            else
                fila=0;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
        }
      return fila;
    }
}