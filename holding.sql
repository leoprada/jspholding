-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         10.1.37-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para holding
CREATE DATABASE IF NOT EXISTS `holding` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `holding`;

-- Volcando estructura para tabla holding.asesor
CREATE TABLE IF NOT EXISTS `asesor` (
  `idasesor` int(11) NOT NULL,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  PRIMARY KEY (`idasesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla holding.asesor: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `asesor` DISABLE KEYS */;
INSERT INTO `asesor` (`idasesor`, `nombres`, `apellidos`, `direccion`, `telefono`, `titulo`, `estado`) VALUES
	(1, 'Leo', 'Apellidos', 'Direccion', 'Telefono', 'Titulo', '0'),
	(2, 'Nombre', 'Apellidos', 'Direccion', 'Telefono', 'Titulo', '1'),
	(3, 'Prueba', 'Apellidos', 'Direccion', 'Telefono', 'Titulo', '1'),
	(4, 'ana', 'nnn', 'nnn', 'nnn', 'abogada', '1');
/*!40000 ALTER TABLE `asesor` ENABLE KEYS */;

-- Volcando estructura para tabla holding.roll
CREATE TABLE IF NOT EXISTS `roll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla holding.roll: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `roll` DISABLE KEYS */;
INSERT INTO `roll` (`id`, `nombre`) VALUES
	(1, 'ASESOR'),
	(2, 'ADMINISTRADOR');
/*!40000 ALTER TABLE `roll` ENABLE KEYS */;

-- Volcando estructura para tabla holding.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `clave` varchar(18) NOT NULL,
  `nombre` varchar(18) NOT NULL,
  `apellidos` varchar(18) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rol_id` (`rol_id`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `roll` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla holding.usuario: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `email`, `rol_id`, `clave`, `nombre`, `apellidos`) VALUES
	(1, 'leo@gmail.com', 2, '12345', 'Leonardo', 'Prada'),
	(2, 'usuario@gmail.com', 1, '12345', 'Usuario', 'De Fulano');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
