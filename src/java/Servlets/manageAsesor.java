/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Modelos.ContAsesor;
import Modelos.ModAsesor;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;

/**
 *
 * @author LEO
 */
@WebServlet(name = "manageAsesor", urlPatterns = {"/manageAsesor"})
public class manageAsesor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        
        int id;
        try (PrintWriter out = response.getWriter()) {
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
            response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            
            ContAsesor Nuevo = new ContAsesor();
            ModAsesor Asesor = new ModAsesor();
            
            
            if (Nuevo.Conexion()) {
                System.out.println("ok");
            } else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                System.out.println("error");
            }
            
            String operacion = request.getParameter("operacion");
            

            switch (operacion) {
                case ("listar"):
                    out.print(Nuevo.MostrarTodo());
                    break;
                case ("consultar"):
                    id = Integer.parseInt(request.getParameter("id"));
                    Asesor=Nuevo.MostrarunArea1(id);
                    if(Asesor != null)
                    {
                        JSONArray json;
                        json = new JSONArray();
                        json.add(Integer.toString(Asesor.getIdasesor()));
                        json.add(Asesor.getNombre());
                        json.add(Asesor.getApellidos());
                        json.add(Asesor.getDireccion());
                        json.add(Asesor.getTelefono());
                        json.add(Asesor.getTitulo());
                        json.add(String.valueOf(Asesor.getEstado()));
                        out.print(json);
                    }
                    else
                    {
                        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        out.print("No encontrado");
                    }
                    Nuevo.Cerrar();
                    break;
                case ("crear"):
                    try {
                        if(Nuevo.Conexion())
                        {
                            Asesor.setIdasesor(Integer.parseInt(request.getParameter("Idasesor")));
                            Asesor.setNombre(request.getParameter("Nombre"));
                            Asesor.setApellidos(request.getParameter("Apellidos"));
                            Asesor.setTelefono(request.getParameter("Telefono"));
                            Asesor.setDireccion(request.getParameter("Direccion"));
                            Asesor.setTitulo(request.getParameter("Titulo"));
                            if (Nuevo.InsertarArea(Asesor)) {
                                out.print("Ingreso correcto");
                            } else {
                                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                                out.print("Error en el ingreso");
                            }
                            Nuevo.Cerrar();
                        }
                    } catch (ClassNotFoundException | NumberFormatException | SQLException e) {
                        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        System.out.println(e);}
                    break;
                case ("modificar"):
                    int fila;
                    try {
                        if(Nuevo.Conexion())
                        {
                            Asesor.setIdasesor((Integer.parseInt(request.getParameter("Idasesor"))));
                            Asesor.setNombre(request.getParameter("Nombre"));
                            Asesor.setApellidos(request.getParameter("Apellidos"));
                            Asesor.setDireccion(request.getParameter("Direccion"));
                            Asesor.setTelefono(request.getParameter("Telefono"));
                            Asesor.setTitulo(request.getParameter("Titulo"));
                            Asesor.setEstado(request.getParameter("Estado").charAt(0));
                            fila=Nuevo.ModificarArea1(Asesor);
                            if(fila !=0 )
                                out.print("Modificación exitosa");
                            else
                            {
                                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                                out.print("No es modificable");
                            }
                            Nuevo.Cerrar();
                        }
                    } catch (ClassNotFoundException | NumberFormatException | SQLException e) {
                        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        System.out.println(e);
                        out.print("Error");
                    }
                    break;
                case ("eliminar"):
                    try {
                        int returned;
                        id = Integer.parseInt(request.getParameter("Idasesor"));
                        if(Nuevo.Conexion())
                        {
                            returned=Nuevo.BorrarArea(id);
                            if(returned != 0)
                            {
                                out.print("Eliminación exitosa");
                            }
                            else {
                                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                                out.print("Hubo un error en la eliminación");
                            }
                        }
                        Nuevo.Cerrar();
                    } catch (ClassNotFoundException | NumberFormatException | SQLException e) {
                        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        System.out.println(e);
                    }
                    break;
            }
            
        }
        catch(Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
            //  Block of code to handle errors
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(manageAsesor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(manageAsesor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
