package Modelos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class ContAsesor 
{
    private Connection cnx;
    
    public boolean Conexion() throws ClassNotFoundException 
    {
       boolean conecto=false;
       try{
           Class.forName("com.mysql.jdbc.Driver");
           String db = "jdbc:mysql://localhost/holding";
           cnx = DriverManager.getConnection(db, "root", "");
           conecto=true;
       }catch(SQLException e)
       {
           e.getMessage();
           System.out.println(e);
       }
       return conecto;
    }
    
    public void Cerrar() throws SQLException
    {
        cnx.close();
    }
    
    public Boolean InsertarArea(ModAsesor Nuevo)
    {
        try {
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            int fila;
            int id;
            String nombre, apellidos, telefono, direccion, titulo;
            char estado;
               
            id=Nuevo.getIdasesor();
            nombre=Nuevo.getNombre();
            apellidos=Nuevo.getApellidos();
            telefono=Nuevo.getTelefono();
            direccion=Nuevo.getDireccion();
            titulo=Nuevo.getTitulo();
            estado = Nuevo.getEstado();
            
            sentencia = "Select * from Asesor where idasesor=" + id;
            
            tabla=sql.executeQuery(sentencia);
            if(!tabla.next())
            {
                sentencia = "INSERT INTO asesor (idasesor, nombres, apellidos, telefono, direccion, titulo, estado) "
                    + "VALUES ('" + id + "', '"+ nombre +"', '"+ apellidos +"', '"+ telefono +"', '"+ direccion +"', '"+ titulo +"', '"+ estado +"')";
                 fila=sql.executeUpdate(sentencia);
                 if(fila == 1)
                     return true;
            }
            else
            {
                System.out.println("Este ID ya existe");
                return false;
            }
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
            return false;
        }
        return false;
    }
    
    
     public int BorrarArea(int id)
     {
        int fila = 0;
         try{
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            sentencia="Select * from Asesor where idasesor="+ id;
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                 sentencia="Delete from Asesor where idasesor="+ id;
                 fila=sql.executeUpdate(sentencia);
                 return fila;
            }
            else
                System.out.println("Id no existe");
            return fila;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
            return fila;
        }
     }
     
     public void MostrarunArea(int id)
     {
         try{
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;

            String nombre, descripcion;
            
            sentencia="Select * from Area where idarea="+ id;
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                nombre=tabla.getString("Nombre");
                descripcion=tabla.getString("Descripcion");
                System.out.println("Nombre: "+ nombre);
                System.out.println("Descripción: "+ descripcion);
            }
            else
                System.out.println("Id no existe");
        } catch (SQLException e) 
        {
            e.getErrorCode();
        }
     }
     
    
     public ModAsesor MostrarunArea1(int id)
     {
         ModAsesor Nuevo=new ModAsesor(); 
         try{
            Statement sql;
                sql = cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            sentencia="Select * from asesor where idasesor='" + id + "'";
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                Nuevo.setIdasesor(Integer.parseInt(tabla.getString("idasesor")));
                Nuevo.setNombre(tabla.getString("NOMBRES"));
                Nuevo.setApellidos(tabla.getString("APELLIDOS"));
                Nuevo.setDireccion(tabla.getString("DIRECCION"));
                Nuevo.setTelefono(tabla.getString("TELEFONO"));
                Nuevo.setTitulo(tabla.getString("TITULO"));
                Nuevo.setEstado(tabla.getString("ESTADO").charAt(0));
            }
            else
                Nuevo=null;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
        }  
       return Nuevo;
     }
     
     public JSONArray MostrarTodo()
     {
        JSONArray tablaJSON = new JSONArray();
       try{
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            int idasesor;
            String nombre, apellidos;
            
            sentencia="Select * from asesor";
            tabla=sql.executeQuery(sentencia);
            
            while(tabla.next())
            {
                JSONObject obj = new JSONObject();
                nombre = tabla.getString("NOMBRES");
                apellidos = tabla.getString("APELLIDOS");
                idasesor = Integer.parseInt(tabla.getString("idasesor"));
                obj.put("nombre", nombre);
                obj.put("apellidos", apellidos);
                obj.put("idasesor", idasesor);
                tablaJSON.add(obj);
            }
            return tablaJSON;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            return tablaJSON;
        }
     }
     
     public int ModificarArea1(ModAsesor Nuevo)
    {
      int fila=0;  
      try {
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            int id;
            String nombres, apellidos, telefono, direccion, titulo;
            char estado;
               
            id=Nuevo.getIdasesor();
            nombres=Nuevo.getNombre();
            apellidos=Nuevo.getApellidos();
            telefono=Nuevo.getTelefono();
            direccion=Nuevo.getDireccion();
            titulo=Nuevo.getTitulo();
            estado=Nuevo.getEstado();
            
            sentencia="Select * from Asesor where idasesor="+ id;
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                sentencia = "INSERT INTO asesor (idasesor, nombres, apellidos, telefono, direccion, titulo, estado) "
                    + "VALUES ('" + id + "', '"+ nombres +"', '"+ apellidos +"', '"+ telefono +"', '"+ direccion +"', '"+ titulo +"', '"+ estado +"')";
                
                sentencia="Update Asesor set nombres='" + nombres +"', " +
                "apellidos='" + apellidos +"', " +
                "direccion='" + direccion +"', " +
                "titulo='" + titulo +"', " +
                "telefono='" + telefono +"', " +
                "estado='" + estado +"'" +
                " where idasesor=" + id;
                
                fila=sql.executeUpdate(sentencia);
            }
            else
                fila=0;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
        }
      return fila;
    }
}