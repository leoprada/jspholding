package Modelos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Datos {
    private Connection cnn;
    
    public Datos() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String db = "jdbc:mysql://localhost/holding";
            cnn = DriverManager.getConnection(db, "root", "");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cerrarConexion() {
        try {
            cnn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String[] validarUsuario(String email, String clave) {
        String [] returnable;
        try {
            String sql = "SELECT * FROM usuario WHERE email = '" + email + "' and clave = '" + clave + "'";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                returnable = new String[4];
                returnable[0] = rs.getString("email");
                returnable[1] = rs.getString("rol_id");
                returnable[2] = rs.getString("nombre");
                returnable[3] = rs.getString("apellidos");
                return returnable;
            } else {
                returnable = new String[1];
                return returnable;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            returnable = new String[1];
            returnable[0] = ex.toString();
            return returnable;
        }
    }
}
