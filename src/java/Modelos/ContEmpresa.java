package Modelos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class ContEmpresa 
{
    private Connection cnx;
    
    public boolean Conexion() throws ClassNotFoundException 
    {
       boolean conecto=false;
       try{
           Class.forName("com.mysql.jdbc.Driver");
           String db = "jdbc:mysql://localhost/holding";
           cnx = DriverManager.getConnection(db, "root", "");
           conecto=true;
       }catch(SQLException e)
       {
           e.getMessage();
           System.out.println(e);
       }
       return conecto;
    }
    
    public void Cerrar() throws SQLException
    {
        cnx.close();
    }
    
    public Boolean InsertarArea(ModEmpresa Nuevo)
    {
        try {
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            int fila;
            String nombre;
               
            nombre=Nuevo.getNombre();
            sentencia = "INSERT INTO empresa (nombre) "
                + "VALUES ('" + nombre + "')";
             fila=sql.executeUpdate(sentencia);
             if(fila == 1)
                 return true;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
            return false;
        }
        return false;
    }
    
    
     public int BorrarArea(int id)
     {
        int fila = 0;
         try{
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            sentencia="Select * from empresa where idempresa="+ id;
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                 sentencia="Delete from empresa where idempresa="+ id;
                 fila=sql.executeUpdate(sentencia);
                 return fila;
            }
            else
                System.out.println("Id no existe");
            return fila;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
            return fila;
        }
     }
     
    
     public JSONArray MostrarunArea1(int id)
     {
         JSONArray tablaJSON = new JSONArray();
         try{
            Statement sql;
                sql = cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            sentencia="Select * from empresa where idempresa='" + id + "'";
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                tablaJSON.add(Integer.parseInt(tabla.getString("idempresa")));
                tablaJSON.add(tabla.getString("nombre"));
            }
            //else
            //    Nuevo=null;
            } catch (SQLException e) 
            {
                e.getErrorCode();
                System.out.println(e);
            } 
        return tablaJSON;
     }
     
     public JSONArray MostrarTodo()
     {
        JSONArray tablaJSON = new JSONArray();
       try{
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            int idempresa;
            String nombre;
            
            sentencia="Select * from empresa";
            tabla=sql.executeQuery(sentencia);
            
            while(tabla.next())
            {
                JSONObject obj = new JSONObject();
                idempresa = Integer.parseInt(tabla.getString("idempresa"));
                nombre = tabla.getString("nombre");
                obj.put("idempresa", idempresa);
                obj.put("nombre", nombre);
                tablaJSON.add(obj);
            }
            return tablaJSON;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            return tablaJSON;
        }
     }
     
     public int ModificarArea1(ModEmpresa Nuevo)
    {
      int fila=0;  
      try {
            Statement sql=cnx.createStatement();
            String sentencia;
            ResultSet tabla;
            
            int idempresa;
            String nombre;
               
            idempresa=Nuevo.getIdempresa();
            nombre=Nuevo.getNombre();
               
            
            sentencia="Select * from empresa where idempresa="+ idempresa;
            tabla=sql.executeQuery(sentencia);
            if(tabla.next())
            {
                sentencia="Update empresa set idempresa='" + idempresa +"', " +
                "nombre='" + nombre +"'" +
                " where idempresa=" + idempresa;
                
                fila=sql.executeUpdate(sentencia);
            }
            else
                fila=0;
        } catch (SQLException e) 
        {
            e.getErrorCode();
            System.out.println(e);
        }
      return fila;
    }
}